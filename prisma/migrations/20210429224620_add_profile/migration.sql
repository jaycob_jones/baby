-- AlterTable
ALTER TABLE "User" ADD COLUMN     "phone" VARCHAR(255),
ADD COLUMN     "phoneValidated" BOOLEAN NOT NULL DEFAULT false;
