import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { firstName, lastName, email, attending, numberOfGuest, comments } =
      req.body;

    const data = await prisma.baby
      .create({
        data: {
          firstName,
          lastName,
          email,
          attending,
          numberOfGuest,
          comments,
        },
      })
      .catch((err) => {
        res
          .status(200)
          .send({ message: "This email has already been used", err });
      });
    res.status(200).send(data);
  }
  if (req.method === "GET") {
    const data = await prisma.baby.findMany();
    res.status(200).send(data);
  }
}
