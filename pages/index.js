import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div
      className={styles.container}
      style={{
        backgroundImage: `url("${require("../public/watercolor-bg.jpg")}")`,
        width: "100%",
        height: "50px",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPositionX: "50%",
      }}
    >
      <Head>
        <title>Gender Reveal RSVP</title>

        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Image
          src="/HeShe.svg"
          alt="Picture of the author"
          width={350}
          height={350}
        />

        <p className={styles.description}>
          Jaycob &amp; Kelsey would like you to join them to find out what their
          baby will be.
        </p>

        <div className={styles.grid}>
          <Link href="/rsvp">
            <a className={styles.card}>
              <h2>RSVP &rarr;</h2>
              <p>
                If you can or can't make it, please click here to RSVP
                <br />
              </p>
              <br />
              <small>click to RSVP</small>
            </a>
          </Link>
          <a
            target="_blank"
            href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=MW5lMGV2aDkzbjE4bmNxZm9la2c5N3ZsYXIgam9uZXNqYXljb2JAbQ&amp;tmsrc=jonesjaycob%40gmail.com"
            className={styles.card}
          >
            <h2>Date and Time &rarr;</h2>
            <p>
              July 10, 2021 <br /> At 5:00 pm
            </p>
            <br />
            <small>click to add to your calender</small>
          </a>
          <a
            href="https://goo.gl/maps/gQmhUhC1zCePcs8X9"
            target="_blank"
            className={styles.card}
          >
            <h2>Location &rarr;</h2>
            <p>
              4847 Jacmel Ct,
              <br /> Sparks NV 89436
            </p>
            <br />
            <small>click for directions</small>
          </a>

          <a className={styles.card}>
            <h2>Food and Drinks </h2>
            <p>
              Come hungry and thirsty because food and drinks will be available.
            </p>
            <br />
            <small>&nbsp;</small>
          </a>
        </div>
      </main>
    </div>
  );
}
